﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.ModelAttributes;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Meep.Tech.Data.Serialization {

  /// <summary>
  /// Plugin to help Serializer serialize to a remote postgreSQL db.
  /// </summary>
  public static class ISerializeable_PostgreSqlSaveAndLoadLogic {

    /// <summary>
    /// Settings to set before configuring.
    /// </summary>
    public static class Settings {

      /// <summary>
      /// The main DB name that this saves to
      /// </summary>
      public static string DBName = "Models.db";
      public static string DBHost = "localhost";
      public static string DBUser = "";
      public static string DBPass = "";
    }

    /// <summary>
    /// Used to split columns in a list
    /// </summary>
    public const string SqlListDelimiter = ", \n\t";

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, string> PostgreSqlColumnDataTypeMappings {
      get;
    } = new Dictionary<Type, string> {
      {typeof(byte[]), "BLOB" },
      {typeof(int), "INTEGER" },
      {typeof(byte), "INTEGER" },
      {typeof(bool), "INTEGER" },
      {typeof(short), "INTEGER" },
      {typeof(string), "TEXT" },
      {typeof(DateTime), "TEXT" },
      {typeof(decimal), "TEXT" },
      {typeof(float), "REAL" },
      {typeof(double), "REAL" }
    };

    /// <summary>
    /// Used to map C# data types to SQL field table types
    /// </summary>
    public static Dictionary<Type, System.Data.DbType> SQLColumnDataTypeMappings {
      get;
    } = new Dictionary<Type, System.Data.DbType> {
      {typeof(byte[]), System.Data.DbType.Binary },
      {typeof(int), System.Data.DbType.Int64 },
      {typeof(byte), System.Data.DbType.Byte },
      {typeof(bool), System.Data.DbType.Boolean },
      {typeof(short), System.Data.DbType.Int16 },
      {typeof(string), System.Data.DbType.String },
      {typeof(DateTime), System.Data.DbType.DateTime },
      {typeof(decimal), System.Data.DbType.Decimal },
      {typeof(float), System.Data.DbType.Decimal },
      {typeof(double), System.Data.DbType.Decimal }
    };

    /// <summary>
    /// The db connection
    /// </summary>
    public static NpgsqlConnection ModelDBConnection {
      get;
      internal set;
    }

    /// <summary>
    /// Call before using the serializer.
    /// </summary>
    public static void Configure() {
      ModelDBConnection = new NpgsqlConnection(
        $"Host={Settings.DBHost};Database={Settings.DBPass}"
        + (!string.IsNullOrEmpty(Settings.DBUser)
          ? $"Username ={ Settings.DBUser };"
          :"")
        + (!string.IsNullOrEmpty(Settings.DBPass)
          ? $"Password ={ Settings.DBPass };"
          :"")
      );
      ModelDBConnection.Open();

      Serializer.Settings.Storage.FetchDataForModelLogic = GetModelData;
      Serializer.Settings.Storage.FetchDataForChildModelsLogic = GetDataForChildModels;
      Serializer.Settings.Storage.FetchDataForModelImageLogic = GetDataForModelImage;
      Serializer.Settings.Storage.SaveDatasForModelsLogic = SaveModelDatas;
    }

    #region DB Access

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    static Serializer.SerializedData GetModelData(string modelUniqueId, Type modelType) {
      string modelTableName = Serializer.Cache.GetDataModelInfo(modelType).TableName;
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Serializer.Cache.GetFieldsInfo(modelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);

      return GetModelData(modelUniqueId, modelTableName, sqlFieldsForThisModelType);
    }

    /// <summary>
    /// Get the model based on ID without child data
    /// </summary>
    static Serializer.SerializedData GetModelData(
      string modelUniqueId,
      string modelTableName,
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
    ) {
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumns, string tableAssuranceQuery, _) = GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumns.Join(SqlListDelimiter) + @"
          FROM " + modelTableName + " " +
         "WHERE id = @id";
      query.Parameters.AddWithValue("id", modelUniqueId);
      NpgsqlDataReader modelDataRow = query.ExecuteReader();
      Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
      JObject modelJSON = new JObject();
      if (modelDataRow.Read()) {
        int columnOrdinalIndex = 0;
        // go though each column we put in and grab the result:
        foreach ((MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) field in sqlFieldsForThisModelType) {
          sqlDataRows.Add(field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name, modelDataRow.GetValue(columnOrdinalIndex++));
        }

        /// Json should be the last column fetched, named 'data'.
        modelJSON = JObject.Parse(modelDataRow.GetString(columnOrdinalIndex));
      }

      return (modelTableName, sqlDataRows, modelJSON);
    }

    /// <summary>
    /// Get all the model data for a single child model type:
    /// </summary>
    static List<Serializer.SerializedData> GetDataForChildModels(
      string parentModelUniqueId,
      Type childModelType,
      string parentModelFieldName
    ) {
      // Get type data from Serializer.Cache:
      DataModelAttribute childModelInfo = Serializer.Cache.GetDataModelInfo(childModelType);
      string modelTableName = childModelInfo.TableName;
      List<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> childModelFieldsInfo
        = Serializer.Cache.GetFieldsInfo(childModelType);
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
        = childModelFieldsInfo.Where(field => field.dataModelFieldInfo.IsASQLField);

      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) = GetTableAssuranceQuery(modelTableName, sqlFieldsForThisModelType);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumnsToQuery.Join(SqlListDelimiter) + @"
           FROM " + modelTableName + " " +
         "WHERE " + Serializer.SQLOwnerColumnName + @" = @owner
           AND " + Serializer.SQLOwnerModelFieldNameColumnName + " = @field";
      query.Parameters.AddWithValue("owner", parentModelUniqueId);
      query.Parameters.AddWithValue("field", parentModelFieldName);
      NpgsqlDataReader resultingRows = query.ExecuteReader();

      // reach each row into a list item
      List<Serializer.SerializedData> childModelDatas = new List<Serializer.SerializedData>();
      while (resultingRows.Read()) {
        int columnOrdinalIndex = 0;
        // go though each column we put in and grab the result:
        Dictionary<string, object> sqlDataRows = new Dictionary<string, object>();
        foreach ((MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo) field in sqlFieldsForThisModelType) {
          sqlDataRows.Add(field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name, resultingRows.GetValue(columnOrdinalIndex++));
        }

        /// Json should be the last column fetched, named 'data':
        childModelDatas.Add((
          modelTableName,
          sqlDataRows,
          JObject.Parse(resultingRows.GetString(columnOrdinalIndex))
        ));
      }

      return childModelDatas;
    }

    /// <summary>
    /// Get the image for a model given the field the image is from
    /// </summary>
    static Dictionary<string, object> GetDataForModelImage(IUnique model, string imageFieldName) {
      Dictionary<string, object> sqlRowData = new Dictionary<string, object>();
      string modelTypeName = Serializer.Cache.GetDataModelInfo(model).TableName;

      // Make the sql command to fetch the image data
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      (IEnumerable<string> sqlColumnsToQuery, string tableAssuranceQuery, _) = GetTableAssuranceQuery(ModelImageFieldAttribute.ImageTableName, null);
      query.CommandText = tableAssuranceQuery
        + "SELECT " +
            sqlColumnsToQuery + @"
           FROM " + ModelImageFieldAttribute.ImageTableName + " " +
         $"WHERE {ModelImageFieldAttribute.ImageOwnerColumnName} = @imageOwner " +
           $"AND {ModelImageFieldAttribute.ImageOwnerTypeColumnName} = @imageType " +
           $"AND {ModelImageFieldAttribute.ImageOwnerFieldColumnName} = @imageField;";
      query.Parameters.AddWithValue("imageOwner", model.id);
      query.Parameters.AddWithValue("imageType", modelTypeName);
      query.Parameters.AddWithValue("imageField", imageFieldName);

      // execute and read the query results:
      NpgsqlDataReader resultingRow = query.ExecuteReader();
      if (resultingRow.Read()) {
        int columnIndex = 0;
        foreach (string columnName in sqlColumnsToQuery) {
          sqlRowData.Add(columnName, resultingRow.GetValue(columnIndex++));
        }
      }

      return sqlRowData;
    }


    /// <summary>
    /// Save serialized model data to the db
    /// </summary>
    static void SaveModelDatas(Dictionary<string, Dictionary<string, IEnumerable<Serializer.SerializedData>>> serializedModelDatas) {
      int textParamId = 0;
      int paramNameId = 0;
      // Lets build a BIG query lol
      NpgsqlCommand query = ModelDBConnection.CreateCommand();
      StringBuilder queryString = new StringBuilder();

      // batch all the models that are in the same tables:
      string currentTable = null;
      IEnumerable<string> currentTableSqlColumnNames = default;
      Dictionary<string, Type> expectedColumnTypes = default;

      /// for each table we need to add model data rows for:
      foreach ((string tableName, Dictionary<string, IEnumerable<Serializer.SerializedData>> serializedModelDatasById) in serializedModelDatas) {

        // if there's a query for another table already in progress we need to finish it:
        if (currentTable != null) {
          queryString.Append(";\n");
        }
        currentTable = tableName;

        // make sure to initialize this table:
        string tableAssuranceQuery;
        (currentTableSqlColumnNames, tableAssuranceQuery, expectedColumnTypes)
          = GetTableAssuranceQuery(currentTable, Serializer.Cache.GetSqlFieldsForTable(tableName));
        queryString.Append($"\n{tableAssuranceQuery}");

        // check if we have deletes for this table based on a parent id. If we do, do those first for the table:
        if (serializedModelDatasById.TryGetValue(Serializer.SerializedDataCollection.DeleteDataKey, out IEnumerable<Serializer.SerializedData> tableDeletionDatas)) {
          foreach (Serializer.SerializedData deletionData in tableDeletionDatas) {
            queryString.Append($"DELETE FROM {deletionData.tableName}\n");
            queryString.Append($"   WHERE {Serializer.SQLOwnerColumnName} = @param_{textParamId++}\n");
            queryString.Append($"      AND {Serializer.SQLOwnerModelFieldNameColumnName} = @param_{textParamId++};\n\n");

            query.Parameters.Add(new NpgsqlParameter {
              ParameterName = $"param_{ paramNameId++ }", 
              DbType = System.Data.DbType.String, 
              Value = deletionData.sqlData[Serializer.SQLOwnerColumnName],
              SourceColumn = Serializer.SQLOwnerColumnName 
            });
            query.Parameters.Add(new NpgsqlParameter{
              ParameterName = $"param_{paramNameId++}",
              DbType = System.Data.DbType.String, 
              Value = deletionData.sqlData[Serializer.SQLOwnerModelFieldNameColumnName],
              SourceColumn = Serializer.SQLOwnerModelFieldNameColumnName 
            });
          }

          // Remove the deletes:
          serializedModelDatasById.Remove(Serializer.SerializedDataCollection.DeleteDataKey);

          // if this was just a delete, continue without adding values
          if (!serializedModelDatasById.Any()) {
            continue;
          }
        }

        /// Add the rows as VALUEs
        // Initialize the upsert:
        queryString.Append($"INSERT INTO {currentTable} (\n\t");
        queryString.Append(currentTableSqlColumnNames.Join(SqlListDelimiter));
        queryString.Append($"\n) VALUES");

        // foreach set of model data under this id key add a row to the DB:
        int currentModelCount = 0;
        int totalModels = serializedModelDatasById.SelectMany(values => values.Value).Count();
        foreach ((string modelId, IEnumerable<Serializer.SerializedData> serializedDatas) in serializedModelDatasById) {
          foreach (Serializer.SerializedData serializedData in serializedDatas) {
            if (currentModelCount++ != 0) {
              queryString.Append(",");
            }
            queryString.Append($" (\n\t");
            int currentRowAddedCount = 0;

            /// For each row of data to add in VALUES:
            // get all the columns and values we're using.
            currentTableSqlColumnNames
              // use null and rely on the DB to provide a default value:
              .Select(columnName => (name: columnName, value: serializedData.sqlData.TryGetValue(columnName, out object columnValue)
                ? columnValue
                : null))
              // add json data to the end:
              .SkipLast(1)
              .Append((Serializer.SQLJSONDataColumnName, serializedData.jsonData?.ToString()))
              .ForEach(column => {
                /// For each column of data to add in VALUES:
                // add the VALUES ?s and +1 ? for json data too
                queryString.Append($"@param_{ textParamId++ }");
                // delimit if this isn't the last item in the list:
                if (++currentRowAddedCount != currentTableSqlColumnNames.Count()) {
                  queryString.Append(SqlListDelimiter);
                } // if it is the last item:
                else {
                  // if this is a model by unique id we want to upsert, not insert:
                  if (currentModelCount == totalModels && currentTableSqlColumnNames.First() == Serializer.SQLIDColumnName) {
                    // TODO: add a toggle that allows: on conflict, use JSON_MERGE_PATCH() to patch json changes onto the new one, to keep removed mod data.
                    queryString.Append($"\n) ON CONFLICT ({Serializer.SQLIDColumnName}) DO UPDATE SET\n");
                    int currentColumn = 1;
                    currentTableSqlColumnNames.Skip(1).ForEach(column => {
                      queryString.Append("\t");
                      queryString.Append(column);
                      queryString.Append(" = excluded.");
                      queryString.Append(column);
                      queryString.Append(++currentColumn != currentTableSqlColumnNames.Count() ? ",\n" : "");
                    });
                  } // the on conflict doesn't need a closing bracket, Values does
                  else {
                    queryString.Append($"\n)");
                  }
                }

                /// try to set up the sqlite param:
                try {
                  NpgsqlParameter param;
                  try {
                    param = new NpgsqlParameter { 
                      ParameterName = $"param_{ paramNameId++ }", 
                      DbType = SQLColumnDataTypeMappings[expectedColumnTypes[column.name]], 
                      Value = column.value ?? DBNull.Value,
                      SourceColumn = column.name
                    };
                  } catch (Exception e) {
                    throw new InvalidCastException($"Could not make a new SqliteParameter for value object of type: {column.value?.GetType().FullName ?? "null"}, with value: {column.value?.ToString() ?? "null"}. Tried to cast to DBtype: {(SQLColumnDataTypeMappings.TryGetValue(column.value?.GetType(), out var dbType) ? dbType.ToString() : "NO DBTYPE FOR THIS TYPE")}\n{e}");
                  }

                  query.Parameters.Add(param);
                } catch (InvalidCastException e) {
                  throw new InvalidCastException($"Could not cast object: {column.value?.ToString() ?? "null"}, of type: {column.value?.GetType().FullName ?? "null"}, to a type SQL can use.\n\nQuery:{query.CommandText}\n\nCurrentParamIndex:{query.Parameters.Count}\n\n{e}");
                }
              });
          }
        }
      }

      // Finalize and build the command string:
      queryString.Append(";");
      query.CommandText = queryString.ToString();

      string queryText = query.CommandText;

      /*foreach (SqliteParameter p in query.Parameters) {
        queryText = ReplaceFirst(
          queryText,
          string.IsNullOrEmpty(p.ParameterName) ? "?" : p.ParameterName,
          p.DbType == DbType.Int64 ? p.Value.ToString() : $"'{p.Value.ToString()}'"
        );
      }

      File.WriteAllText(Application.persistentDataPath + "/test", queryText);//query.CommandText + "\n\nvalues:\n\n" + query.Parameters.Cast<SqliteParameter>().Select(param => $"{(string.IsNullOrEmpty(param.SourceColumn) ? "?" : $"?{param.SourceColumn}")}::{param.Value?.ToString() ?? "null"}").Join("\n"));
      Debug.Log("Successfuly Built Query. Running...");
      Debug.Log($"Query Finished! {query.ExecuteNonQuery()} rows effected.");*/
    }

    /// <summary>
    /// testing, remove
    /// </summary>
    /// <param name="text"></param>
    /// <param name="search"></param>
    /// <param name="replace"></param>
    /// <returns></returns>
    static string ReplaceFirst(string text, string search, string replace) {
      int pos = text.IndexOf(search);
      if (pos < 0) {
        return text;
      }
      return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    #endregion

    #region Cache

    /// <summary>
    /// Serializer.Cached data queries
    /// </summary>
    static readonly Dictionary<string, (IEnumerable<string>, string)> CachedTableQueries
      = new Dictionary<string, (IEnumerable<string>, string)>();

    /// <summary>
    /// Get the columns to fetch for a table and a query to set up the table just in case
    /// </summary>
    static (
      IEnumerable<string> sqlColumnNames,
      string tableCreationAssuranceQuery,
      Dictionary<string, Type> columnDataTypes
    ) GetTableAssuranceQuery(
      string tableName,
      IEnumerable<(MemberInfo objectFieldInfo, ModelDataFieldAttribute dataModelFieldInfo)> sqlFieldsForThisModelType
    ) {
      IEnumerable<string> columnsToFetch;
      Dictionary<string, Type> columns
        = new Dictionary<string, Type>();
      string tableCreationAssuranceQuery;
      if (CachedTableQueries.TryGetValue(tableName, out var tableValues)) {
        columnsToFetch = tableValues.Item1;
        tableCreationAssuranceQuery = tableValues.Item2;
      } else {
        // image table columns:
        if (tableName == ModelImageFieldAttribute.ImageTableName) {
          columns = ModelImageFieldAttribute.ImageTableColumns;
        } // model defined columns:
        else {
          columns = sqlFieldsForThisModelType.ToDictionary(
            field => field.dataModelFieldInfo.Name ?? field.objectFieldInfo.Name,
            field => field.dataModelFieldInfo.CustomSerializeToType ?? field.objectFieldInfo.DataType()
          );
          // data column:
          columns[Serializer.SQLJSONDataColumnName] = typeof(string);
        }

        // query text generation:
        columnsToFetch = columns.Keys;
        tableCreationAssuranceQuery
          = $"CREATE TABLE IF NOT EXISTS {tableName} (\n\t" +
              columns.Select(column => $"{column.Key} {(PostgreSqlColumnDataTypeMappings.TryGetValue(column.Value, out string columnTypeName) ? columnTypeName : throw new NotImplementedException($"type {column.Value} is not recoginized as a SQL column type in SQLColumnDataTypeMappings"))} {(column.Key == Serializer.SQLIDColumnName ? "PRIMARY KEY " : "")}").Join(SqlListDelimiter) +
            $"\n);\n\n";
      }

      CachedTableQueries[tableName] = (columnsToFetch, tableCreationAssuranceQuery);
      return (columnsToFetch, tableCreationAssuranceQuery, columns);
    }

    #endregion
  }
}
