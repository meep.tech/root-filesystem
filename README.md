# Meep-Tech; Intro
This is the root of all meep-tech projects. This file will explain the folder structure and where to put different projects and how to create git repos for new projects.

* Frontend and Backend applications go under the apps folder.
* Re-useable Code Libraries go in the libraries folder.
